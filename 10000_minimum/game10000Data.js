function SaveGameLocaly() {
    window.localStorage.setItem("player1", document.getElementById("in_total1").value);
    window.localStorage.setItem("player2", document.getElementById("in_total2").value);
    alert("The Game is saved");
}

function LoadGameFromLocalStorage() {
    document.getElementById("in_total1").value = window.localStorage.getItem("player1");
    document.getElementById("in_total2").value = window.localStorage.getItem("player2");
    alert("The Game is ready");
}