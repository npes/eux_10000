# EUX_10000

## Her kan findes kode og dokumenter relateret til EUX forløbet på UCL i emnet "programmering"

Forløbet afvikles som et 3 dages kursus:
* Dag 1: Interaktionsdesign
* Dag 2: Introduktion til programmering. HTML, CSS og JavaScript
* Dag 3: Implementering af spillet 10000 med HTML, CSS og JavaScript

Link til spillet: https://npes.gitlab.io/10000_game